"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Users {
    createListUsers() {
        this.listUsers = [
            {
                'name': 'admin',
                'password': 'admin',
                'role': 'admin',
                'roleid': 1
            },
            {
                'name': 'autentia',
                'password': 'autentia',
                'role': 'plain_user',
                'roleid': 2
            },
            {
                'name': 'angular',
                'password': 'angular',
                'role': 'little_user',
                'roleid': 3
            }
        ];
    }
    constructor() {
        this.createListUsers();
    }
    authUser(username, password) {
        let trazeni = this.listUsers.filter((item) => {
            return item.name == username && item.password == password;
        });
        return (trazeni.length > 0) ?
            {
                isAuthenticated: true,
                roleid: trazeni[0].roleid
            } :
            {
                isAuthenticated: false
            };
    }
}
exports.Users = Users;
//# sourceMappingURL=users.js.map