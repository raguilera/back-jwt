/// <reference path="typings/index.d.ts" />
import * as express from 'express';
import * as morgan from 'morgan';
import * as cors from 'cors';
import { apiRoutes } from './routes/apiRoutes';
import { MyToken } from './mytoken';

export class App {
    private _app: express.Express;
    private _port: number;
    private _globalToken: MyToken;
    private _router: apiRoutes;

    constructor(port: number) {
        this._app = express();
        this._port = port;
        this._globalToken = new MyToken();
        this._router = new apiRoutes(this._globalToken);
    }

    public listen() {

        var allowedOrigins = ['http://localhost:4200',
            'http://localhost:9876', 'http://localhost:49152'];
        this._app.use(cors({
            origin: function (origin, callback) {
                console.log(origin);
                // allow requests with no origin 
                // (like mobile apps or curl requests)
                if (!origin) return callback(null, true);
                if (allowedOrigins.indexOf(origin) === -1) {
                    var msg = 'The CORS policy for this site does not ' +
                        'allow access from the specified Origin.';
                    return callback(new Error(msg), false);
                }
                return callback(null, true);
            },
            credentials: true,
            optionsSuccessStatus: 200
        }));

        this._app.use('/', this._router.returnRouter());

        console.log("Server is listening at " + this._port);
        this._app.listen(this._port);
    }

}


