"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="typings/index.d.ts" />
const express = require("express");
const cors = require("cors");
const apiRoutes_1 = require("./routes/apiRoutes");
const mytoken_1 = require("./mytoken");
class App {
    constructor(port) {
        this._app = express();
        this._port = port;
        this._globalToken = new mytoken_1.MyToken();
        this._router = new apiRoutes_1.apiRoutes(this._globalToken);
    }
    listen() {
        var allowedOrigins = ['http://localhost:4200',
            'http://localhost:9876', 'http://localhost:49152'];
        this._app.use(cors({
            origin: function (origin, callback) {
                console.log(origin);
                // allow requests with no origin 
                // (like mobile apps or curl requests)
                if (!origin)
                    return callback(null, true);
                if (allowedOrigins.indexOf(origin) === -1) {
                    var msg = 'The CORS policy for this site does not ' +
                        'allow access from the specified Origin.';
                    return callback(new Error(msg), false);
                }
                return callback(null, true);
            },
            credentials: true,
            optionsSuccessStatus: 200
        }));
        this._app.use('/', this._router.returnRouter());
        console.log("Server is listening at " + this._port);
        this._app.listen(this._port);
    }
}
exports.App = App;
//# sourceMappingURL=app.js.map