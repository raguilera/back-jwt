"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const users_1 = require("../data/users");
const express = require("express");
class apiRoutesLevel1 {
    constructor() {
        this._router = express.Router();
        this._router.use((req, res, next) => {
            if (req.cookies['decodedToken'].permissions == 1) {
                next();
            }
            else {
                //user authenticated but unauthorized
                return res.status(403).json({
                    success: false,
                    status: "User unauthorized...Need level 1"
                });
            }
        });
        this._router.get('/list/users', function (req, res, next) {
            res.status(200).json({
                success: true,
                data: users_1.LIST_USERS
            });
        });
    }
    returnRouter() {
        return this._router;
    }
}
exports.apiRoutesLevel1 = apiRoutesLevel1;
//# sourceMappingURL=apiRoutesLevel1.js.map